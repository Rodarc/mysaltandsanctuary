extends "res://Enemys/Enemy.gd"

enum States {Idle, Walk, Attack, Roll, Damage, Defense, Died, Jump}
enum IAStates {WalkToEnemy, Fighting, AttackEnemy, Idle, DefenseEnemy, Died}
var current_state = States.Idle
var current_ia_state = IAStates.Idle

onready var IABT = get_node("BehaviorTree")
var BTStatus_attack = NodeBT.StatusBT.Ready # tener este estado, esta mal, buscar alguna alternativa

func _ready():
    #instanciar las armas, esto no estara aqui, solo por ahora
    if RightWeapon:
        right_weapon = RightWeapon.instance()
        get_node("Visuals/RightHand").add_child(right_weapon)
        right_weapon.connect("contact", self, "on_contact_right_weapon")
    if LeftWeapon:
        left_weapon = LeftWeapon.instance()
        get_node("Visuals/LeftHand").add_child(left_weapon)
    ConstuctBeahviourTree()

func evets_process(delta):
    match current_state:
        States.Idle:
            # o comrpuebo aquie que haya un enemigo, o hago que la funcion encargada me avise
            pass
        States.Attack:
            pass

func _process(delta):
    IABT.execute(delta)

func _physics_process(delta):
    #movemente.y += GRAVITY * delta # esto deberia aplicarlo solo si estoy en el aire
    # ia_proccess(delta)
    # IABT.execute(delta)
    match current_state:
        States.Idle:
            if energy < max_energy: # la regarga debe iniciar cierto tiempo despues de haber terminar de realizar una accion
                energy = clamp(energy + rate_energy_recovery * delta, 0, max_energy)
            movement.y = 0
            if move_direction.x:
                change_state(States.Walk)
            else:
                move_and_slide(movement, Vector2(0.0, -1.0))
        States.Walk:
            # modificacion al movivimiento, si estoy mirando a la derecha, y e muevo a laizquierda, deeria moverme com oretrocidiendo
            # si el valor del eje de mover izquierda super cierto umbral alto, el personaje girara hacia la izquierda, y caminara mirando esa direccion, se invierto todo en ese momento.
            # esto solo funciona con un analógico, con el teclado no funcionaria
            if energy < max_energy:
                energy = clamp(energy + rate_energy_recovery * delta, 0, max_energy)
            
            if !is_on_floor():
                change_state(States.Jump)
            #move_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
            # establecer la direccion de movimiento
            if move_direction.x > 0:
                if orientation != 1:
                    orientation = 1
                    get_node("Visuals").scale.x = 1
                    #global_scale.x = 1
                    #set_global_scale(Vector2(1.0, 1.0))
            elif move_direction.x < 0:
                if orientation != -1:
                    orientation = -1
                    #global_scale.x = -1
                    get_node("Visuals").scale.x = -1
                    #set_global_scale(Vector2(-1.0, 1.0))
            #print("orientation ", orientation)
            #print("scale ", global_scale.x)
            movement.x = move_direction.x * movement_max_speed
            movement.y = Globals.Gravity * delta
            
            move_and_slide(movement, Vector2(0.0, -1.0))
        States.Jump:
            movement.y += Globals.Gravity * delta
            movement.x = move_direction.x * movement_max_speed
            move_and_slide(movement, Vector2(0.0, -1.0))
            if is_on_floor():
                change_state(States.Idle)
                
        States.Defense:
            # puedo moverme en este estado
            # puedo iniciar un ataque
            # puedo hacer un parry
            # puedo intentar saltar
            # si ataco meintras y sigo presionando el boton de defensa, debo regresar a ese estado atuomaticamente, sin tener que presionar nueamente , ya que ya eta presionado 
            
            movement.x = move_direction.x * movement_max_speed
            movement.y = Globals.Gravity * delta
            move_and_slide(movement, Vector2(0.0, -1.0))
        
        

func enter_state(state):
    #incia las animaciones correspondietes, etc
    #cambia parametros
    match state: # o current_state
        States.Idle:
            movement = Vector2(0.0, 0.0)
        States.Attack:
            movement = Vector2()
        States.Damage:
            movement = Vector2()
            get_node("AnimationPlayer").play("Damaged")
        States.Died:
            movement = Vector2()
            emit_signal("died")
            # ejecutar animacion de morir, que inicia como recibiendo un ligero golpe.
            # emito la señal de morir!!, inician ciertas animaciones en el juego y eventos luego de morir, com oel reiniico, el letro de has muerto
        States.Defense: #defense Idle
            movement = Vector2()
            get_node("AnimationPlayer").play("Defense")
            # Dos opciones
            # manejar dos animaciones, una para levantar el escudo, y otra para bajar el escudo
            # la primera animacino incia al entrar en el estado y levantar el escudo.
            # la segunda animacion inicia al soltar el boton de defensa, baja el escudo despues de haber salido del estado, o tal vez salga del estado despues de bajar el escudo.
            # 
            # la segunda opcion es tener una animacion, cuando llegue a la mitad hacerle stop
            # cuando suelte el boton reanudamos la animacion, puede que ahi inie el sieuginet estao o no
            #
            # los estados deben estar asociados a animaciones? o independientes?
            # diria que debe ser tener dos animaciones
            # y cuando este con el escudo levantado, la animacion de caminar ya no sera la misma
            # sera otra, nuevo vonjunto de animaciones
            # o nuevo conjuto de estados, defense-walk, defenes-jump al menos el defense walk si o si
        States.Damage:
            movement = Vector2()
            get_node("AnimationPlayer").play("Damaged")
    pass

func exit_state(state):
    #puede hacer delays si son necesarios.
    match state:
        States.Idle:
            pass
        States.Defense:
            # debo iniciar la animacion de bajar defensa y cuando esta termine recien entrar al siguente estado o cuando suelte el boton, inicio la animacion y cuando termine llamo a cambiar estado
            # es posible que que una aniacion no este ligada a un estado? por ejemplo si bajo el escudo, pero empiezo a moverme, es necesario que termine la animcion de bajar el escudo? ahi dira que no, quiza inicar alguna animacion de transicion, para el caso de la defensa es sufiiento, no para el ataque
            pass
    pass

func change_state(new_state):
    exit_state(current_state)
    print("enemy state: ", new_state)
    current_state = new_state#deberia estar dentro de enterstate?
    enter_state(new_state)

func enter_ia_state(state):
    match state:
        IAStates.WalkToEnemy:
            change_state(States.Walk)
        IAStates.Fighting:
            change_state(States.Idle)
        IAStates.Idle:
            move_direction.x = 0.0
            change_state(States.Idle)
    pass

func exit_ia_state(state):
    pass

func change_ia_state(new_state):
    exit_ia_state(current_ia_state)
    print("enemy ia state: ", new_state)
    current_ia_state = new_state#deberia estar dentro de enterstate?
    enter_ia_state(new_state)

func attack():
    # debo verificar que tenga energia suficiente para realizar el ataque
    # si tengo ataco, si no tengo, no ataco, si tuviera cierto minimo, podria hacer un mal ataque
    if energy > energy_for_attack:
        energy = clamp(energy - energy_for_attack, 0, max_energy) # podria encapsular esto en un a funcion, sobre todo por que tambien trendre que hacer los emiters
        change_state(States.Attack)
        get_node("AnimationPlayer").play("Attack")
    
        attack_time = get_node("AnimationPlayer").get_animation("Attack").length # tengo la duracion
        #en realidad al asignarse el arma, el tiempo debe cambiar, sobre todo por que se ajusta a las propiedades de fuera, para ver que tan rapido se ejecuta la animacion
        #por ahora, el tiempo de ataque es igual que el de la animacion
    
        #iniciar animacion, y manejar tiempos para el combo, en funcion de eso, inicio la segunda animacion
        get_node("TweenAttack").interpolate_callback(self, anticipation_time, "activate_collision_attack")
        get_node("TweenAttack").start()
   

func defense_down():
    get_node("AnimationPlayer").play("Defense_down")
    deactivate_left_collision()
    change_state(States.Idle)

func activate_collision_attack():
    #acitvo la colision del arma, llamando a una funcion de la misma
    #inicio el tewn para que llame a descativar
    if right_weapon:
        print("activando arma")
        right_weapon.activate_collision()
        get_node("TweenAttack").interpolate_callback(self, strike_time, "deactivate_collision_attack")
    pass


func deactivate_collision_attack():
    print("desactivando arma")
    if right_weapon:
        right_weapon.deactivate_collision()
        #podria usar tewwn para que cuando pase el tiempo de recovery, cambie al estado de idle, 
    pass


func activate_left_collision():
    if left_weapon:
        left_weapon.activate_collision()
    pass

func deactivate_left_collision():
    if left_weapon:
        left_weapon.deactivate_collision()


func move_stop():
    pass

func move_left():
    # la ia llama a esta funcion cuando tenga que iniciar el movimineto en esta direccion
    # solo cambia las velocidad y la direccion para que se mueve a la izqueirad
    # para detenerme la ia debe llamar a la funcion stop
    move_direction.x = -1.0
    pass

func move_right():
    move_direction.x = 1.0
    pass

func ia_proccess(delta):
    match current_ia_state:
        IAStates.WalkToEnemy:
            if EnemyTarget:
                var Distancia: Vector2 = EnemyTarget.position - position
                # si la distancia es menor a cierto numero, entro en el estado de pelea
                if Distancia.length() <= 100.0:
                    change_ia_state(IAStates.Fighting)
                if Distancia.x > 0: # y yo no me estoy moviendo a ese lado (falta esta parte)
                    move_right()
                elif Distancia.x < 0:
                    move_left()
        IAStates.Fighting:
            #podria alternar entre ataque y defensa
            if EnemyTarget:
                var Distancia: Vector2 = EnemyTarget.position - position
                if Distancia.length() > 100.0 && current_state != States.Attack:# Cuidar las cancelaciones, deberia ejecutar esto solo cuando no estoy en determinado estado, quiza hace falta un switch
                    change_ia_state(IAStates.WalkToEnemy)
                else:
                    attack()
            # si esta en rango intento atacar, quiza varios seguidos
            # si el enemigo se me esta acercando, activo el escudo
            # si no tengo energia desactivo la defensa
            # faltan delays
            # todo este procesamiento deberia estar en otro lado
            pass


func _on_DetectionRange_body_entered(body):
    ._on_DetectionRange_body_entered(body)
    # llamando al padre
    if body.has_method("take_damage"):
        # walking to 
        change_ia_state(IAStates.WalkToEnemy);

func _on_DetectionRange_body_exited(body):
    ._on_DetectionRange_body_exited(body)
    if body.has_method("take_damage"):
        # walking to 
        change_ia_state(IAStates.Idle);


func _on_AnimationPlayer_animation_finished(anim_name):
    match anim_name:
        "Attack":
            # quiza deberia ver que no haya inciado otra, asumo que para eso es change animation, si he llegaodo a eto, quiere dicri que ni inicie algun ataque, ni recibi ataque, que cortar la animacion de ataque
            change_state(States.Idle)
            BTStatus_attack = NodeBT.StatusBT.Completed
        "Damaged":
            change_state(States.Idle)
    
func on_contact_right_weapon(actor):
    actor.call("take_damage", 30)

func take_damage(damage):
    .take_damage(damage)
    if health:
        change_state(States.Damage)

func BTPatrol(delta):
    pass
    
func BTChase(delta):
    pass

func BTWalkToEnemy(delta):#este deberia recibir parametro hacia que enemigo, sin emabro qquien esta haciedo esteo se el propio detect enemy, internamente, mejorar esta comunicacion
    if EnemyTarget:
        var Distancia: Vector2 = EnemyTarget.position - position
        # si la distancia es menor a cierto numero, entro en el estado de pelea
        if Distancia.x > 0: # y yo no me estoy moviendo a ese lado (falta esta parte)
            move_right()
        elif Distancia.x < 0:
            move_left()
        if Distancia.length() <= 100.0:
            return NodeBT.StatusBT.Completed
        return NodeBT.StatusBT.Running
        # deberia retornar Completed, si me he ubicado lo suficientemente cerca del objetivo, quiza hasta que entre en colision con el o algo
    return NodeBT.StatusBT.Failed


func BTDetectEnemyTarget(delta):
    return EnemyTarget != null

func BTIsEnemyInAttackRange(delta):
    if EnemyTarget:
        var Distancia: Vector2 = EnemyTarget.position - position
        # si la distancia es menor a cierto numero, entro en el estado de pelea
        return Distancia.length() <= 100.0
    return false

func BTAttack(delta): # necesito tener un estado para el atque, saber si ya termine y retornar completed en ese momento
    if (current_state != States.Attack): # la comprobacion de energia se debe hacer en el arbol, no aqui, ya que la cantidad de energia determina la estrategia tambien, deberia estar en ambos lados
        attack()
        BTStatus_attack = NodeBT.StatusBT.Running
    return BTStatus_attack

func ConstuctBeahviourTree():
    if IABT:
        IABT.root = SelectorNodeBT.new()
        var node = SequenceNodeBT.new()
        node.add_child(ConditionalNodeBT.new())
        node.add_child(ActionNodeBT.new())
        node.childs[0].Condition = funcref(self, "BTIsEnemyInAttackRange")
        node.childs[1].Action = funcref(self, "BTAttack")
        IABT.root.add_child(node)
        node = SequenceNodeBT.new()
        node.add_child(ConditionalNodeBT.new())
        node.add_child(ActionNodeBT.new())
        node.childs[0].Condition = funcref(self, "BTDetectEnemyTarget")
        node.childs[1].Action = funcref(self, "BTWalkToEnemy")
        IABT.root.add_child(node)