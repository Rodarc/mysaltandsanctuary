extends KinematicBody2D

#enum States {Idle, Walk, Attack, Roll, Damage, Defense, Died}
#var current_state = States.Idle

export var max_health: float = 150.0
var health: float = max_health

export var max_energy: float = 100.0
var energy: float = max_energy
export var rate_energy_recovery: float = 25.0

signal died
signal health_changed

var move_direction: Vector2 = Vector2() #direccion a al a que me movere, variable para ser alterada por los inputs y la gravedad
export var movement_max_speed: float = 250.0

var movement: Vector2 = Vector2()

export (PackedScene) var RightWeapon #quiza esto este en otro lado
export (PackedScene) var LeftWeapon

var right_weapon: Area2D = null
var left_weapon: Area2D = null

onready var orientation: int = scale.x

var EnemyTarget = null

var anticipation_time: float = 0.15
var strike_time: float = 0.35
var recovery_time: float = 0.5
var attack_time: float
var energy_for_attack: float = 20.0

func _on_DetectionRange_body_entered(body):
    if body.has_method("take_damage"):
        EnemyTarget = body


func _on_DetectionRange_body_exited(body):
    if body == EnemyTarget:
        EnemyTarget = null


func take_damage(damage: float):
    #cuando puedo recibir dañó, y cuando no?
    #por erro podria tener una doble validacion?
    #si recibio ataque mientras me estan atacando, debo iniciar una nueva animacon de ataque, pueod esto?
    health = clamp(health - damage, 0, max_health)
    emit_signal("health_changed", health)
    if !health:
        queue_free()
        # change_state(States.Died)
    else:
        pass
        # change_state(States.Damage)
        # esto no hara que se ejecute nuevamente la animacion, o si?, probar
        # en caso de que no lo haga, aqui debo iniciar nuevamente las animacion
    
    #inciar la animaion de recibir dañó
    #actualizar la vida
    #se regrsa al estado idle, despues de terminar
    #estas animaciones estan funcionando com timers a la ver, tener cuidado con esto
    #ver la correcta secuencia de eventos para que todo sea comodo y pueda recivbir daño
    pass