extends CanvasLayer

onready var health_bar = get_node("MarginContainer/VBoxContainer/HealthBar")
onready var energy_bar = get_node("MarginContainer/VBoxContainer/EnergyBar")

func _ready():
    var player_max_health = get_node("../Player").max_health
    health_bar.max_value = player_max_health
    update_health(player_max_health)
    get_node("../Player").connect("health_changed", self, "update_health")
    var player_max_energy = get_node("../Player").max_energy
    energy_bar.max_value = player_max_energy
    update_energy(player_max_energy)
    get_node("../Player").connect("energy_changed", self, "update_energy")

func update_health(new_health):
    health_bar.value = new_health

func update_energy(new_energy):
    energy_bar.value = new_energy