extends Area2D

enum UtilityTypes {Attack, Defese}
enum WeaponTypes {Sword, Shield}

export (WeaponTypes) var Type
export (UtilityTypes) var Utility

signal contact

func activate_collision():
    print("Arma activada")
    get_node("CollisionShape2D").disabled = false
    pass


func deactivate_collision():
    print("Arma desactivada")
    get_node("CollisionShape2D").disabled = true
    pass


func is_collision_active() -> bool:
    return !get_node("CollisionShape2D").disabled

#cuando entre en contacto con algo, debo emitir una señál, y pasar datos, como el enemigo con el que entre en contacto
#el que sostenga esta arma, debe conectarse a esta señal, y recibir el dato, entces este le palicara los dañós y demas.
#el arma se asegura de no mandar al mismo pesronaje arias veces, sol uan vez durante el ataque.
#cuando el que sostiene inica un ataque, el arma  debe resetear lo que tenia com colision, quiz deshabilitar y ahbilitar la coisiona un frame
# o aglo  para evitar el caso en que el nemigo sea muy grande y como estoy atacandolo puede que siempre este en contacto con la espada
#podria deshabilitar la colision, durante el la fase de, stand by wind up, y la de recovery, y que solo este activa en la fase de followtrhoug

func _on_Weapon_body_entered(body):
    if body.has_method("take_damage"):
        # print(body)
        emit_signal("contact", body)
