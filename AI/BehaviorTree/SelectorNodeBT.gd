extends NodeBT

class_name SelectorNodeBT
# Selector o Priority

func _ready():
    pass

func execute(delta):
    var new_status = StatusBT.Failed
    var i = 0
    while (new_status == StatusBT.Failed && i < childs.size()):
        new_status = childs[i].execute(delta)
        print("[SelectorBT]: child ", i, " ", NameStatusBT[new_status])
        i += 1
    status = new_status
    print("[SelectorBT] ", NameStatusBT[status])
    return status
    #for node in childs:
        #node.execute(delta)