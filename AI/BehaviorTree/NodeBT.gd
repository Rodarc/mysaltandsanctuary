class_name NodeBT

enum StatusBT {Ready, Running, Completed, Failed, Aborted}
var NameStatusBT = ["Ready", "Running", "Completed", "Failed", "Aborted"]

# estado del nodo
var status = StatusBT.Ready

# referencia al padre
var parent: NodeBT = null

# referencia a los hijos
var childs: Array = []

func execute(delta):
    pass

func add_child(new_child: NodeBT):
    childs.push_back(new_child);

func add_child_in_position(new_child: NodeBT, position: int):
    if position > 0 && position < childs.size():
        childs.insert(position, new_child)