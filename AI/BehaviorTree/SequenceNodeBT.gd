extends NodeBT

class_name SequenceNodeBT

func _ready():
    pass
    
func execute(delta):
    var new_status = StatusBT.Completed
    var i = 0
    while (new_status != StatusBT.Failed && i < childs.size()):
        var child_status = childs[i].execute(delta)
        print("[SelectorBT]: child ", i, " ", NameStatusBT[child_status])
        if child_status != StatusBT.Completed:
            new_status = child_status
        i += 1
    status = new_status
    print("[SequenceBT] ", NameStatusBT[status])
    return status
    #for node in childs:
        #node.execute(delta)
