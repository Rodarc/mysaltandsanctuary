extends KinematicBody2D

enum States {Idle, Walk, Jump, Fall, Attack, Roll, Climb, Damage, Defense, Died, Grab}
var current_state = States.Idle

var max_health: float = 200.0
var health: float = max_health

var max_energy: float = 100.0
var energy: float = max_energy
var rate_energy_recovery: float = 25.0
# funciones gain and drain energy

var energy_for_roll: float = 20.0

signal died
signal health_changed
signal energy_changed

var move_direction: Vector2 = Vector2() #direccion a al a que me movere, variable para ser alterada por los inputs y la gravedad
export var movement_max_speed: float = 250.0
export var roll_max_speed: float = 150.0

var movement: Vector2 = Vector2()

export var climb_velocity: float = 300.0

export var jump_force: float = 500.0

var grabed_platform: StaticBody2D = null #podria ser el static body, pero en realidad es quuien tenga el let_pass
var grabed_corner: Area2D = null

export (PackedScene) var RightWeapon #quiza esto este en otro lado
export (PackedScene) var LeftWeapon

var right_weapon: Area2D = null
var left_weapon: Area2D = null

onready var orientation: int = scale.x

var path_selector = null

#onready var TweenAttack = get_node("TweenAttack")
# el tween atack debe ser detenido, totalmente en caso de haber recibido daño o que algo me haya interrumpido durante el ataque
# por ahora se esta controlando con el animation player

# estos timpos en realidad son fracciones de segundo, segun el ataque que se realiza
# esto para corrdinar con la duracion de la animacion, y establecer en que momento esta activada la colision
# es decir los valores van de 0 a 1
var anticipation_time: float = 0.15
var strike_time: float = 0.35
var recovery_time: float = 0.5
var attack_time: float
var energy_for_attack: float = 20.0

# variables para el grabclimb
var GrabPositionInital
var GrabPositionMiddle
var GrabPositionFinal
var GrabPositionActual
var actualTimeGrabPosition : float
var grab_first_fase : bool
var grab_second_fase : bool


func _ready():
    # instanciar las armas, esto no estara aqui, solo por ahora
    if RightWeapon:
        right_weapon = RightWeapon.instance()
        get_node("Visuals/RightHand").add_child(right_weapon)
        right_weapon.connect("contact", self, "on_contact_right_weapon")
        
    if LeftWeapon:
        left_weapon = LeftWeapon.instance()
        get_node("Visuals/LeftHand").add_child(left_weapon)
    change_state(States.Idle)

func _physics_process(delta):
    #movemente.y += GRAVITY * delta # esto deberia aplicarlo solo si estoy en el aire
    match current_state:
        States.Idle:
            if energy < max_energy: # la regarga debe iniciar cierto tiempo despues de haber terminar de realizar una accion
                energy = clamp(energy + rate_energy_recovery * delta, 0, max_energy)
                emit_signal("energy_changed", energy)                
            movement.y = 0
            if Input.is_action_pressed("move_right") || Input.is_action_pressed("move_left"):
                change_state(States.Walk)
            elif Input.is_action_just_pressed("jump"):
                if Input.is_action_pressed("move_down"):
                    for i in get_slide_count():
                        var collision = get_slide_collision(i)
                        if collision.collider.has_method("let_pass"):
                            #.call("let_pass", self)
                            down_of_platform(collision.collider)
                else:
                    jump()

            # si entre a uno de estos if, ya no deberia ejecutar el codigo de abajo
            elif Input.is_action_just_pressed("weak_attack"):
                attack()
            elif Input.is_action_just_pressed("evade"):
                roll()
            elif Input.is_action_just_pressed("defense"):
                #()
                change_state(States.Defense)
            else:
                move_and_slide(movement, Vector2(0.0, -1.0))
                if !is_on_floor():
                    change_state(States.Jump)
                #state fallin o jumping
        States.Walk:
            # modificacion al movivimiento, si estoy mirando a la derecha, y e muevo a laizquierda, deeria moverme com oretrocidiendo
            # si el valor del eje de mover izquierda super cierto umbral alto, el personaje girara hacia la izquierda, y caminara mirando esa direccion, se invierto todo en ese momento.
            # esto solo funciona con un analógico, con el teclado no funcionaria
            if energy < max_energy:
                energy = clamp(energy + rate_energy_recovery * delta, 0, max_energy)
                emit_signal("energy_changed", energy)
            if !Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_left"):
                change_state(States.Idle)
            elif Input.is_action_just_pressed("jump"):
                if Input.is_action_pressed("move_down"):
                    for i in get_slide_count():
                        var collision = get_slide_collision(i)
                        if collision.collider.has_method("let_pass"):
                            #.call("let_pass", self)
                            down_of_platform(collision.collider)
                else:
                    jump()
            elif Input.is_action_just_pressed("weak_attack"):
                attack()
            elif Input.is_action_just_pressed("evade"):
                roll()
            else:
                if !is_on_floor():
                    change_state(States.Jump)
                #move_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
                var move_to_right: float = Input.get_action_strength("move_right")
                var move_to_left: float = Input.get_action_strength("move_left")
                if move_to_right > move_to_left:
                    move_direction.x = move_to_right
                elif move_to_left > move_to_right:
                    move_direction.x = -move_to_left
                else:
                    move_direction.x = 0
                # algo esta mal con la logica, no con el procesmiento de la entrada
                # print("move direction ", move_direction.x)
                if move_direction.x > 0:
                    if orientation != 1:
                        orientation = 1
                        get_node("Visuals").scale.x = 1
                        #global_scale.x = 1
                        #set_global_scale(Vector2(1.0, 1.0))
                elif move_direction.x < 0:
                    if orientation != -1:
                        orientation = -1
                        #global_scale.x = -1
                        get_node("Visuals").scale.x = -1
                        #set_global_scale(Vector2(-1.0, 1.0))
                #print("orientation ", orientation)
                #print("scale ", global_scale.x)
                movement.x = move_direction.x * movement_max_speed
                movement.y = Globals.Gravity * delta
                var move_to_up: float = Input.get_action_strength("move_up")
                var move_to_down: float = Input.get_action_strength("move_down")
                if(path_selector): # si estoy en un path selector, debo mandarle mi direccion de movimiento para deshabilite o habilite algunas plataformas
                    var move_y
                    if move_to_up > move_to_down:
                        move_y = -move_to_up
                    elif move_to_down > move_to_up:
                        move_y = move_to_down
                    else:
                        move_y = 0
                    var direction_input = Vector2(move_direction.x, move_y).normalized()
                    print("direction_input: ", direction_input)
                    path_selector.call("let_pass_in_direction", self, direction_input)
                move_and_slide(movement, Vector2(0.0, -1.0))
        States.Jump:
            movement.y += Globals.Gravity * delta
            # if movement.y > 0:
                # get_node("AnimationPlayer").play("Fall")
            move_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
            movement.x = move_direction.x * movement_max_speed
            move_and_slide(movement, Vector2(0.0, -1.0))
            if is_on_floor():
                change_state(States.Idle)
            else: #estoy en el aire
                if Input.is_action_pressed("move_up"):
                    #print("try to climb")
                    if grabed_platform:
                        climb()
                        print("climb!!!")
                    #inicio el climb
                if grabed_corner && grabed_corner.let_grab(orientation, move_direction.x):
                    grab()
                    print("Grabed")
                # if Input.is_action_pressed("move_right") && orientation == 1:
                    # if grabed_corner:# deberia tener en cuenta la direccion del corner
                        # grab()
                        # print("Grabed")
                # elif Input.is_action_pressed("move_left") && orientation == -1:
                    #if grabed_corner:
                        # grab()
                        # print("Grabed")
        States.Climb:
            movement.y = -climb_velocity
            movement.x = 0.0
            move_and_slide(movement, Vector2(0.0, -1.0))
            #hacer esto hasta que super la plataforma sujetada, es decir deje de colisionar con ella
            if !grabed_platform:
                movement.y = 0.0
                change_state(States.Jump)
            pass
        States.Grab:
            # Moverme en y hasta alcanzar la altura del corner, y luego recien mover en x hasta pasar el x del corner
            # entonces salir del estado, esta salida ya no depende de la animacion, depende de haber terminado el movimiento
            # a que velocidad debo moverme?
            actualTimeGrabPosition += delta # esta variacion deberia ser en funcion de la velocidad a la que suba, por ahora lo dejare como que subo todo en un segundo
            if grab_first_fase:
                var new_position: Vector2 = lerp(GrabPositionInital, GrabPositionMiddle, actualTimeGrabPosition)
                position = new_position
                if(actualTimeGrabPosition >= 1.0):
                    grab_first_fase = false
                    grab_second_fase = true
                    actualTimeGrabPosition = 0.0
            elif grab_second_fase:
                var new_position: Vector2 = lerp(GrabPositionMiddle, GrabPositionFinal, actualTimeGrabPosition)
                position = new_position
                if(actualTimeGrabPosition >= 1.0):
                    grab_second_fase = false
                    change_state(States.Idle)
            
            pass
        States.Defense:
            # puedo moverme en este estado
            # puedo iniciar un ataque
            # puedo hacer un parry
            # puedo intentar saltar
            # si ataco meintras y sigo presionando el boton de defensa, debo regresar a ese estado atuomaticamente, sin tener que presionar nueamente , ya que ya eta presionado 
            if Input.is_action_just_pressed("weak_attack"):
                attack()
                #la animacion de ataque debe deshacer la defensa, es decir debe bajar el escudo
            elif Input.is_action_just_released("defense"):
                # o iniciar animacion de bajada y entrar en idle
                #get_node("AnimationPlayer").play("Defense", -1, -1, true) # esto podria estar en el exit
                get_node("AnimationPlayer").play("Defense_down")
                deactivate_left_collision()
                change_state(States.Idle)
            else:
                if !is_on_floor():
                    change_state(States.Jump)
                move_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
                movement.x = move_direction.x * movement_max_speed
                movement.y = Globals.Gravity * delta
                move_and_slide(movement, Vector2(0.0, -1.0))
        States.Damage:
            #debo quiza retroceder, la colision junto con la animacion
            # no hago mas nada, salgo, que este en el aire, tener cuidado con ese estado tambien, quiza siempre este moviendome hacia abajo
            # se sale de este estado solo, cuano termne el tiempo de recepcion de dañó
            pass
        States.Roll:
            # hago rol en la direccion que a la que apunto con el analógico
            # debo a vanzar hacia alli, sin importar si sigo presionando o no
            movement.x = move_direction.x * roll_max_speed
            #movement.y = Globals.Gravity * delta
            if is_on_floor():
                movement.y = Globals.Gravity * delta
            else:
                movement.y += Globals.Gravity * delta    
            move_and_slide(movement, Vector2(0.0, -1.0))
            
            pass


func _input(event):
    #match current_state:
        #States.Idle:
    #if event.is_action_pressed("move_right"):
    
    #elif event.is_action_pressed("move_left"):
    #ove_direction.x -= event.get_action_strength("move_left")
        #move_direction.x = 1.0
    #print(move_direction)
    pass   

func enter_state(state):
    #incia las animaciones correspondietes, etc
    #cambia parametros
    match state: # o current_state
        States.Idle:
            get_node("AnimationPlayer").play("Idle")
            movement = Vector2(0.0, 0.0)
        States.Attack:
            movement = Vector2()
        States.Jump:
            #movement.y = 0.0
            get_node("AnimationPlayer").play("Jump")
        States.Fall:
            get_node("AnimationPlayer").play("Fall")
        States.Damage:
            movement = Vector2()
            get_node("AnimationPlayer").play("Damaged")
        States.Died:
            movement = Vector2()
            emit_signal("died")
            # ejecutar animacion de morir, que inicia como recibiendo un ligero golpe.
            # emito la señal de morir!!, inician ciertas animaciones en el juego y eventos luego de morir, com oel reiniico, el letro de has muerto
        States.Defense: #defense Idle
            movement = Vector2()
            get_node("AnimationPlayer").play("Defense")
            # Dos opciones
            # manejar dos animaciones, una para levantar el escudo, y otra para bajar el escudo
            # la primera animacino incia al entrar en el estado y levantar el escudo.
            # la segunda animacion inicia al soltar el boton de defensa, baja el escudo despues de haber salido del estado, o tal vez salga del estado despues de bajar el escudo.
            # 
            # la segunda opcion es tener una animacion, cuando llegue a la mitad hacerle stop
            # cuando suelte el boton reanudamos la animacion, puede que ahi inie el sieuginet estao o no
            #
            # los estados deben estar asociados a animaciones? o independientes?
            # diria que debe ser tener dos animaciones
            # y cuando este con el escudo levantado, la animacion de caminar ya no sera la misma
            # sera otra, nuevo vonjunto de animaciones
            # o nuevo conjuto de estados, defense-walk, defenes-jump al menos el defense walk si o si
        States.Roll:
            get_node("AnimationPlayer").play("Roll")
        States.Grab:
            # deberia iniciar la animacion de treapar muro, desde el punto donde se entro en colision
            pass
            
    pass

func exit_state(state):
    #puede hacer delays si son necesarios.
    match state:
        States.Idle:
            pass
        States.Defense:
            # debo iniciar la animacion de bajar defensa y cuando esta termine recien entrar al siguente estado o cuando suelte el boton, inicio la animacion y cuando termine llamo a cambiar estado
            # es posible que que una aniacion no este ligada a un estado? por ejemplo si bajo el escudo, pero empiezo a moverme, es necesario que termine la animcion de bajar el escudo? ahi dira que no, quiza inicar alguna animacion de transicion, para el caso de la defensa es sufiiento, no para el ataque
            pass
    pass

func change_state(new_state):
    exit_state(current_state)
    print(new_state)
    current_state = new_state#deberia estar dentro de enterstate?
    enter_state(new_state)

func jump():
    change_state(States.Jump)
    movement.y = -jump_force # esto no esta en el enter state, para poder entrar a este estado cuando caigo o salgo de una plataforma


func climb():
    change_state(States.Climb)
    #podria hacer que suba hasta que ese grabed body salga de mi colision principal
    #este movimiento lo tengo que hacer en una deterinada cantidad de timpo fija, para que siempre coincida con la animacion
    #por lo tanto la velocidad de climb, es variable, toma una determinada cantidad de tiempo
    # una vez que se termina, de pasar, o salio de la colision, debo cambiar de estado, a idle o alto asi o a jump, y limpar el grabde platform
    #aun que si el climb no lo hice al borde del personaje si no al medio, la animacion deberia inicar en la mitad, y el tempo para subir deberia ser otro
    #deberia haber una forma de predecir cuanto necesitare para subir completamente.
    #por ahora subir a verlocidad de climb, hasta terminar la plataforma, si estoy a medio cuerpot, natrualmnte subira en la mitad del tiempo

func grab(): #grab climb
    # necesito un corner activo, se supone que deberia estar establecido, esto debe funcionar como el de la plataforma
    # necestio el rect, para saber el alto y el ancho
    var ancho
    var alto
    # la colision del personaje es una capsula
    var collision = get_node("CollisionShape2D").shape as CapsuleShape2D
    if collision:
        ancho = collision.radius
        alto = collision.height + 2 * collision.radius
    # los calculos deben estar en funcion de la orientacion del grabed corner
    if grabed_corner.orientation == 1:
        GrabPositionInital = Vector2(grabed_corner.position.x - ancho, grabed_corner.position.y + (alto + grabed_corner.alto)) # deberia tambien restar el alto de la region del corner, el extra
        GrabPositionMiddle = Vector2(GrabPositionInital.x, grabed_corner.position.y)
        GrabPositionFinal = Vector2(grabed_corner.position.x + ancho, grabed_corner.position.y)
        GrabPositionActual = Vector2(GrabPositionInital.x, position.y) # altura actual
    elif grabed_corner.orientation == -1:
        GrabPositionInital = Vector2(grabed_corner.position.x + ancho, grabed_corner.position.y + (alto + grabed_corner.alto)) # deberia tambien restar el alto de la region del corner, el extra
        GrabPositionMiddle = Vector2(GrabPositionInital.x, grabed_corner.position.y)
        GrabPositionFinal = Vector2(grabed_corner.position.x - ancho, grabed_corner.position.y)
        GrabPositionActual = Vector2(GrabPositionInital.x, position.y) # altura actual
    
    actualTimeGrabPosition = (position.y - GrabPositionInital.y)/(GrabPositionMiddle.y - GrabPositionInital.y)
    grab_first_fase = true
    change_state(States.Grab)
    

func down_of_platform(platform):
    #se supone que estoy en colision con una plataform
    platform.call("let_pass", self)
    #llamo a su metodo let_pass y caigo, entro en el estado fall
    change_state(States.Jump)

func roll():
    #establezco la direccion en funcion de la direccion que este presionada y/o mi direccion actual
    # puedo usar y seria lo ideal, mi vecto ron, cuando gire, debo hacer mi transform un espjo, que afecte a la izqueirda
    # ver como hacer eso
    if energy > energy_for_roll:
        energy = clamp(energy - energy_for_roll, 0, max_energy) # podria encapsular esto en un a funcion, sobre todo por que tambien trendre que hacer los emiters
        emit_signal("energy_changed", energy)
        change_state(States.Roll)
        get_node("AnimationPlayer").play("Roll", -1, 1.5)
        #falta inciar el timer para el
        # la direccion del roll, deberia ser en uncion del inputo, no de mi orientacion
        # ya que puedo hacer rol hacia atras, esto se puede ar cuando tenga el escudo levantado y este caminando hacia atras
        move_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
        move_direction.x = orientation
        #if move_direction.x > 0.0:#en realidad deberia ser a que direccion estoy mirando, izquierda o derecha, no con el imput,
        #    move_direction.x = 1.0
        #else: 
        #    move_direction.x = -1.0
        #falta desactivar la colision, o cambiar la mascara de colision, para no recibir daño


func attack():
    # debo verificar que tenga energia suficiente para realizar el ataque
    # si tengo ataco, si no tengo, no ataco, si tuviera cierto minimo, podria hacer un mal ataque
    if energy > energy_for_attack:
        energy = clamp(energy - energy_for_attack, 0, max_energy) # podria encapsular esto en un a funcion, sobre todo por que tambien trendre que hacer los emiters
        emit_signal("energy_changed", energy)
        change_state(States.Attack)
        get_node("AnimationPlayer").play("Attack")
    
        attack_time = get_node("AnimationPlayer").get_animation("Attack").length # tengo la duracion
        #en realidad al asignarse el arma, el tiempo debe cambiar, sobre todo por que se ajusta a las propiedades de fuera, para ver que tan rapido se ejecuta la animacion
        #por ahora, el tiempo de ataque es igual que el de la animacion
    
        #iniciar animacion, y manejar tiempos para el combo, en funcion de eso, inicio la segunda animacion
        get_node("TweenAttack").interpolate_callback(self, anticipation_time, "activate_collision_attack")
        get_node("TweenAttack").start()

func activate_collision_attack():
    #acitvo la colision del arma, llamando a una funcion de la misma
    #inicio el tewn para que llame a descativar
    if right_weapon:
        print("activando arma")
        right_weapon.activate_collision()
        get_node("TweenAttack").interpolate_callback(self, strike_time, "deactivate_collision_attack")
    pass


func deactivate_collision_attack():
    print("desactivando arma")
    if right_weapon:
        right_weapon.deactivate_collision()
        #podria usar tewwn para que cuando pase el tiempo de recovery, cambie al estado de idle, 
    pass

func activate_left_collision():
    if left_weapon:
        left_weapon.activate_collision()
    pass

func deactivate_left_collision():
    if left_weapon:
        left_weapon.deactivate_collision()

func take_damage(damage: float):
    #cuando puedo recibir dañó, y cuando no?
    #por erro podria tener una doble validacion?
    #si recibio ataque mientras me estan atacando, debo iniciar una nueva animacon de ataque, pueod esto?
    if current_state != States.Defense:
        health = clamp(health - damage, 0, max_health)
        emit_signal("health_changed", health)
        if !health:
            change_state(States.Died)
        else:
            change_state(States.Damage)
            # esto no hara que se ejecute nuevamente la animacion, o si?, probar
            # en caso de que no lo haga, aqui debo iniciar nuevamente las animacion
    
    #inciar la animaion de recibir dañó
    #actualizar la vida
    #se regrsa al estado idle, despues de terminar
    #estas animaciones estan funcionando com timers a la ver, tener cuidado con esto
    #ver la correcta secuencia de eventos para que todo sea comodo y pueda recivbir daño
    pass


func _on_GrabCollision_area_entered(area):
    """
    Comentado por ahora, conservada por que tiene indeas interesantes.
    Usando el area para detectar los corners, no implica un que este agarrando, pero con esto veo y guardo si tengo un area cerca
    en caso de que presione agarra, si la tengo aguardade me agarro
    """
    #print(area.get_parent())
    # el problema es que no todas las areas tendrian parent
    #var platform = area.get_parent()
    #if platform.has_method("let_pass"):
        #print ("me puedo agarrar a una plataforma area")
        #es una plataforma que puedo atravesr, y por lo tanto puedo sujetarme
        # de alguna forma debo guardar, el actor de esta área y subir, y estar por encima desde el punto que lo agarre
        # recordar que estas plataformas pueden estar inclindas, o quza tener una forma variable con desniveles, podria reducir a diagonles claras y plataformas planas
        # ubicar la posicion del personaje por enicma de esta linea, necesito saber pa posicion en la plataforma donde boy a subir
        # o podria ser donde me este agarrando, pero tengo que encontrr la interseccion, y luego ber en que punto no tengo poblemas con la colision, par que cuando deje de subir si o si este por encima, o no caiga
        # es decir debo pasar completamente la colision de plataforma con completa seguridad
    match current_state:
        States.Jump:
            if area.has_method("let_grab"):
                grabed_corner = area
                print ("me puedo agarrar a una corner")

func _on_GrabCollision_area_exited(area):
    match current_state:
        States.Jump:
            if area == grabed_corner:
                grabed_corner = null
        States.Grab:
            if area == grabed_corner:
                grabed_corner = null


func _on_GrabCollision_body_entered(body):
    # esto solo debe funcionar cuando no estoy en el estado climbing, o solo cuando este en el estado jump
    match current_state:
        States.Jump:
            if body.has_method("let_pass"):
                grabed_platform = body
                print ("me puedo agarrar a una plataforma body")

        #es una plataforma que puedo atravesr, y por lo tanto puedo sujetarme
        # de alguna forma debo guardar, el actor de esta área y subir, y estar por encima desde el punto que lo agarre
        # recordar que estas plataformas pueden estar inclindas, o quza tener una forma variable con desniveles, podria reducir a diagonles claras y plataformas planas
        # ubicar la posicion del personaje por enicma de esta linea, necesito saber pa posicion en la plataforma donde boy a subir
        # o podria ser donde me este agarrando, pero tengo que encontrr la interseccion, y luego ber en que punto no tengo poblemas con la colision, par que cuando deje de subir si o si este por encima, o no caiga
        # es decir debo pasar completamente la colision de plataforma con completa seguridad


func _on_GrabCollision_body_exited(body):
    match current_state:
        States.Jump:
            if body == grabed_platform:
                grabed_platform = null
        States.Climb:
            if body == grabed_platform:
                grabed_platform = null


func _on_AnimationPlayer_animation_finished(anim_name):
    #temporalmente usare la finalizacino de la animaion para saber que he terminado e tacar, no es lo adeuado creo, aun que puede que si, en dark souls nada cancela la animacion, salvo atacar nuevaente en el momento justo o que te peuguen
    #solo deberia saber como resproducri la animacion mas rapido
    match anim_name:
        "Attack":
            # quiza deberia ver que no haya inciado otra, asumo que para eso es change animation, si he llegaodo a eto, quiere dicri que ni inicie algun ataque, ni recibi ataque, que cortar la animacion de ataque
            change_state(States.Idle)
        "Damaged":
            change_state(States.Idle)
        "Defense":
            activate_left_collision()
        "Roll":
            if is_on_floor():
                change_state(States.Idle)
            else:
                change_state(States.Jump)
            # el problema es que luego entro en idle, y de idle pasa a jump, pero cuando entro a idle, la aceleracion de la gravedad se setea en 0, deberia pasar a jump, en lugar de idle, en funcion de mi sutacion


func set_selector(selector):
    # puedo recibir null, cuando salfga de un selector
    print(selector)
    path_selector = selector

func on_contact_right_weapon(actor):
    actor.call("take_damage", 40)
