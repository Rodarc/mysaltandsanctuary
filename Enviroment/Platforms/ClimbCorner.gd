tool
extends Area2D

enum Orientations {Right = 1, Left = -1}

onready var alto = get_node("CollisionShape2D").shape.extents.x * 2
# onready var orientation: int = scale.x # de esta forma se cambia la orientacion en funcion de la escala

export (Orientations) var orientation: int = Orientations.Right setget setOrientation 

func _ready():
    scale.x = orientation

func setOrientation(new_orientation):
    orientation = new_orientation
    scale.x = orientation

func let_grab(grab_orientation, direction_movement):
    if direction_movement > 0:
        direction_movement = 1
    elif direction_movement < 0:
        direction_movement = -1
    return grab_orientation == orientation && direction_movement == orientation
    # esto debe hacer algo? de prongo algun efecto de particulas, pero en realidad no hace nada
    # lo estoy usand solo apra saber que este objetos es un climb corner
    # podria llamar a esta funcion, y preguntalr, puedo agarrarme, pasando mi direccino de movimiento o mi orinetacion
    # o ambas, y en funcion de eso, esta funcion me devuelve verdaro o falso, si puedo agarrarme o no
