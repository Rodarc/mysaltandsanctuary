#tool
extends StaticBody2D

#class_name PlatformPass

var _area: Area2D = null
var _collisionshape: CollisionShape2D = null

func _ready():
    #debo llenar aqui las variables? 
    #if (_area):
    for child in get_children() :
        if child is CollisionShape2D:
            _collisionshape = child as CollisionShape2D
        if(child is Area2D):
            _area = child as Area2D
    print (_area)
    if(_area):
        for childArea in _area.get_children():
            if childArea is CollisionShape2D:
                childArea.shape = _collisionshape.shape
    #if _collisionshape:    
        #var rect = RectangleShape2D.new()
        #rect.extents = Vector2(40, 40)
        #_collisionshape.shape = rect

func let_pass(body):
    #debo añadir excepcion no desactivar la colision, pare evitar que ataques o enemigos caigan, y si hubiera vairos jugadores, solo el de la accion debe caer
    add_collision_exception_with(body)

func dont_let_pass(body):
    remove_collision_exception_with(body)

#func get_class():
    #return "PlatformPlass"

#func _get_configuration_warning():
    #print("configurando")
    #o lleno aqui las variables?
    #var Warning: String = String()
    #var num_childs = get_child_count()
    #var has_area2d: bool = false
    #for child in get_children() :
        #if child is CollisionShape2D:
            #_collisionshape = child as CollisionShape2D
        #if(child is Area2D):
            #_area = child as Area2D
        #has_area2d = has_area2d || child is Area2D
        #if !(child is Area2D):
    #if !has_area2d:
        #Warning = "Se necesita un Area2D\n"
    #return Warning
    
#en la funcion ready, puedo copiar la forma del primer collision shape al del area
#la idea es que me deje pasar, cuando termine de pasar se active nueamente la colision, esto una vez que activo cierta funcion


func _on_PlatformArea_body_exited(body):
    if body in get_collision_exceptions():
        remove_collision_exception_with(body)
    
