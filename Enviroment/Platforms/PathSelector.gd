tool
extends Node2D

var VectorReference: Vector2 = Vector2(0.0, -1.0)

enum Orientations {Right = 1, Left = -1}
export (Orientations) var orientation: int = Orientations.Right setget setOrientation 


enum SelectorTypes {A, B, C, D}
var TypeFeatures = {# diccionario en estilo normal, tomando strings o entenros como claves
    SelectorTypes.A: {num_paths = 2, # diccionario estilo lua, tomando las calves como nombres de variables
                        intervals_input = [{angulo_inicio = 0.0, angulo_final = 69.5}, {angulo_inicio = 69.5, angulo_final = 180.0}],
                        directions = [45.0, 90.0],
                    },
    SelectorTypes.B: {num_paths = 2,
                        intervals_input = [{angulo_inicio = 0.0, angulo_final = 114.5}, {angulo_inicio = 114.5, angulo_final = 180.0}],
                        directions = [90.0, 135.0],
    },
    SelectorTypes.C: {num_paths = 2,
                        intervals_input = [{angulo_inicio = 0.0, angulo_final = 90.0}, {angulo_inicio = 90.0, angulo_final = 180.0}],
                        directions = [45.0, 135.0],
    },
    SelectorTypes.D: {num_paths = 3,
                        intervals_input = [{angulo_inicio = 0.0, angulo_final = 69.5}, {angulo_inicio = 69.5, angulo_final = 114.5}, {angulo_inicio = 114.5, angulo_final = 180.0}],
                        directions = [45.0, 90.0, 135.0],
    },
}

# array con referencias, a los nodos que instanciados
var Paths = []

export (SelectorTypes) var SelectorType setget set_SelectorType

export (PackedScene) var PlataformType
# esta paltaforma debe ser de tamaño fjo y corto, ya que solo sirve de enlace para conectar con otras plataformas, que estan en esa direccion
# probablemente la referencia de estas paltaformas esten a un costa para simplificar el proceso de posicionamiento y rotacion
# por ahora usar las paltaformas que ya tengo hechas
# la ultima platafomra, la plataforma inferior, no necesariamente podria ser atravesable,
# con esto quiero decir que cada plataforma, cada camino, podria ser de un tipo distitno, por emplo, la segunda podria ser un camino solido.


func _ready():
    # setOrientation(Orientations.Right)
    # set_SelectorType(SelectorTypes.A)
    # esto provoca que al inicar el juego carque mas, no usar sets en ready, salvo quiera ello al empezar el juego
    pass

func setOrientation(new_value):
    orientation = new_value
    scale.x = orientation

func set_SelectorType (new_value):
    SelectorType = new_value
    if !Engine.editor_hint: # activo este if para que se haga esta comprobacion solo cuando estoy en el juego y no en el editor
        yield(self, 'tree_entered')
    # debo limpiar los paths, anteriores
    for node in Paths:
        node.queue_free()
    Paths.clear()
    # debo instanciar nuevos paths en funcion del tipo de selector nuevo
    if PlataformType:
        var i = 0
        while i < TypeFeatures[SelectorType].num_paths:
            var path = PlataformType.instance()
            add_child(path)
            Paths.append(path)
            # debo rotar cada path, aplicar un menos noventa al angulo obtenido a paratir de la referencia
            var delta_rotation = deg2rad(TypeFeatures[SelectorType].directions[i] - 90.0)
            path.rotate(delta_rotation)
            # debo trasladar en la direccion cada path, para hacer que los extremos toquen apenas el selector, y todos coincidadn en el mismo punto
            var direction_move = Vector2(1.0, 0.0).rotated(delta_rotation)
            var collisionpath = path.get_node("CollisionShape2D")
            var length = 0
            if collisionpath:
                #if collisionpath.shape is RectangleShape2D:
                var shape = collisionpath.shape as RectangleShape2D
                if shape:
                    length = shape.extents.x
            path.translate(direction_move * length)
            i += 1
    
func let_pass_in_direction(body, move_direction: Vector2):
    # esta funcion se llama continuamente por el cuerpo que esta atravezando este ojeto, le indica costantemente a que direccion va
    # y lo que debo hacer es agregar escepcion o remover la excepcion, en funcion de la direccion.
    # cuanod el objeto salga de la region del selector, debo remover la escepcion de todos los caminos
    # el jugador asi mismo, debe cambiar a null la referencia a este selector, ya que el es quien llama a esta fucnion
    # por defecto debo tener una direccionquiza arriba apra que todas las palataformas esten activadas
    # moe_direction en cierto modo es casi lo que recibe del imputo, pero ya en el espacio del mundo
    # debo calcular el angulo respecto a la referencia, para determinar a que paltaformas les digo que lo dejen pasar
    print (move_direction)
    move_direction.x = move_direction.x * orientation # para adapatar el input  si la orientacion del selector es left
    var angulo_direccion = rad2deg(acos(VectorReference.dot(move_direction)))
    print("angulo direccion ", angulo_direccion)
    # en teoria no necesito saber el signo, ya que si es signo negativo, quiere decir que estoy apuntando al otro, lado, y salvo que este del otro lado, el angulo es el mismo, no le afecta a la seelcino de plataformas
    #var signo = VectorReference.cross(move_direction)
    #if signo < 0:
        #angulo_direccion = 360 - angulo_direccion
        #print("angulo corregido ", angulo_direccion)
    var i = 0
    while i < TypeFeatures[SelectorType].num_paths:
        var interval = TypeFeatures[SelectorType].intervals_input[i]
        if interval.angulo_final <= angulo_direccion:
            #desactivo, o podria comparar solo el angulo final
            if Paths[i].has_method("let_pass"):
                Paths[i].call("let_pass", body)
        else:
            if Paths[i].has_method("dont_let_pass"):
                Paths[i].call("dont_let_pass", body)
        i += 1
        
    pass

# no se si debo filtra esto, para saber que es un actor que se mueve, digamos el jugador, o los enemigos, para evitar tener en cuanta muros, o bombas
# otra opcion es que sea el propio actor interesado, el que notifique que ha entrado en la region
# quiza la region deba tener una mascara, que solo encuentre a jugadores y enemigos no ha entorno
func _on_SelectionArea_body_entered(body):
    # si es alguien como un jugador o un enemigo, le notificare que ha entrado en contacot con un selector, le paso mi referencia
    # y este a partir de ahora me dira en que direccion quiere moverse
    print(body)
    if body.has_method("set_selector"):
        body.call("set_selector", self)
    pass # Replace with function body.


func _on_SelectionArea_body_exited(body):
    # removemos la exclusion de todas las plataformas
    # asi como algunas plataformas tiene la funcion let pass
    # deberia tener una fucnion que haga lo contrario
    for path in Paths:
        if path.has_method("dont_let_pass"):
            path.call("dont_let_pass", body)
    # cambio a nul el selector de este body
    if body.has_method("set_selector"):
        body.call("set_selector", null)
    pass # Replace with function body.
